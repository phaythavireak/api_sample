package com.example.demo.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomLogoutHandler implements LogoutHandler {

    public static final String BEARER_AUTHENTICATION = "Bearer ";
    private final Logger logger = LoggerFactory.getLogger(CustomLogoutHandler.class);

    private final TokenStore tokenStore;

    public CustomLogoutHandler(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String token = request.getHeader("authorization");
        if (token != null && token.startsWith(BEARER_AUTHENTICATION)) {
            OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(StringUtils.substringAfter(token, BEARER_AUTHENTICATION));
            if (oAuth2AccessToken != null) {
                tokenStore.removeAccessToken(oAuth2AccessToken);
                if (oAuth2AccessToken.getRefreshToken() != null)
                    tokenStore.removeRefreshToken(oAuth2AccessToken.getRefreshToken());
                logger.info("========>>> succeed remove token from db");
            } else {
                logger.info("========>>> token not found in db");
            }
            logger.info("========>>> " + token);
        }
    }
}