package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;


import javax.sql.DataSource;

@Configuration
//@PropertySource(value = {"classpath:/config/db.properties"})
public class DatasourceConfig {

    @Autowired
    private Environment env;

 /*   @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.db.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.db.url"));
        dataSource.setUsername(env.getProperty("spring.db.username"));
        dataSource.setPassword(env.getProperty("spring.db.password"));
        initSchema(dataSource);
        return dataSource;
    }
*/
    private void initSchema(DataSource dataSource) {
        // schema init
        Resource initSchema = new ClassPathResource("script/schema-oauth2.sql");
        Resource initData = new ClassPathResource("script/data-oauth2.sql");
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initData);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);
    }

}