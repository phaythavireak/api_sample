package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("resource_identity").stateless(true);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
      /*  http.csrf().disable()
                .anonymous().disable().authorizeRequests()
                .antMatchers("/oauth/**","/login", "/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**",
                        "/swagger-ui.html").permitAll()
                //.antMatchers("/index").hasRole("CLIENT")
                .anyRequest().authenticated()
                .antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.POST, "/**").access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PUT, "/**").access("#oauth2.hasScope('write')")
                .and().logout().addLogoutHandler(new CustomLogoutHandler(tokenStore))
                .logoutSuccessHandler(new CustomLogoutSuccess()).permitAll()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);*/
       http.csrf().disable().authorizeRequests()
        		.antMatchers("/webjars/**").permitAll()
                .antMatchers(HttpMethod.GET,"/swagger-ui.html","/swagger-resources/**", "/v2/api-docs/**", "/configuration/**").permitAll()
                .antMatchers(HttpMethod.POST,"/swagger-ui.html","/swagger-resources/**", "/v2/api-docs/**").permitAll()
                .antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.POST, "/**").access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PUT, "/**").access("#oauth2.hasScope('write')")
      /*          .and().logout().addLogoutHandler(new com.loan.core.api.config.CustomLogoutHandler(tokenStore))
                .logoutSuccessHandler(new com.loan.core.api.config.CustomLogoutSuccess()).permitAll()*/
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


       /* http.anonymous().and()
                .authorizeRequests()
                .antMatchers("/", "/lib/*", "/images/*", "/css/*", "/swagger-ui.js","/swagger-ui.min.js", "/api-docs", "/fonts/*", "/api-docs/*", "/api-docs/default/*", "/o2c.html","index.html","/webjars/**","/hystrix/**").permitAll()
                .antMatchers(HttpMethod.GET, "/my").access("#oauth2.hasScope('my-resource.read')")
                .anyRequest().authenticated();*/
    }



/*


    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.anonymous().and().authorizeRequests()
                .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/**",
                        "/swagger-ui.html", "/webjars/**")
                .permitAll().and().authorizeRequests().anyRequest().authenticated();
    }*/

}