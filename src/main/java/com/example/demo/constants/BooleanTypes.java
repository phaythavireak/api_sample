package com.example.demo.constants;

public enum BooleanTypes {
	YES("Y", true),
	NO("N", false);
	
	private String value;
	private Boolean booleanValue;

	private BooleanTypes(String value, Boolean booleanValue) {
		this.value = value;
		this.booleanValue = booleanValue;
	}

	public final String getValue() {
		return value;
	}
	
	public final Boolean getBooleanValue() {
		return booleanValue;
	}

	public static BooleanTypes instanceOf(String YN) {
		if(YN != null) {
			if(YES.getValue().equalsIgnoreCase(YN)) {
				return BooleanTypes.YES;
			}
			else {
				return BooleanTypes.NO;
			}
		}
		else {
			return null;
		}
	}
	
	public static BooleanTypes instanceOf(boolean YN) {
		if(YN) {
			return BooleanTypes.YES;
		}
		else {
			return BooleanTypes.NO;
		}
	}
}
