package com.example.demo.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Codes {

	
	ERROR("E0001", "System error"),
	ERROR_EXTERNAL("E0012", "External System error"),
	VALIDATION_ERROR("E0002", "System validation error"),
	ERROR_CAN_NOT_FOUND_PHONE("ENF001", "Could not found  phone number"),
	ERROR_DUPLICATE_NAME("E0003", "Duplicated user name"),
	ERROR_DUPLICATE_PHONE("E0004", "Duplicated phone1"),
	ERROR_INVALID_OTP("E0005", "Invalid OTP"),
	ERROR_INVALID_ORDER_DETAIL_ID("EOD01", "Invalid order detail ID"),
	ERROR_INVALID_USER_REQUEST("EU001", "error.invalid.user.request"),
	ERROR_DUPLICATE_USER_LOGIN("EUD01", "error.duplication.user.login"),
	ERROR_INVALID_REQUEST("E0017", "Invalid Request"),
	ERROR_INVALID_PHONE_NUMBER("E0011", "Invalid Phone number"),
	ERROR_INVALID_PHONE_NUMBER_ALREADY("E0013", "Phone number already existing in member"),
	
	INSTALLMENT_VALIDATE("IV001", "Can't update schedule of this contract, Coz there are some installment were paid"),
	
	REPAID_VALIDATE_01("REP01", "This product can't do repaid installment...!"),
	REPAID_VALIDATE_02("REP02", "Repaid can do only the same installment date...!"),
	
	USER_01("USER_01", "There are no profile...!"),
	
	MASTER_CON_01("MASTER_CON_01", "Validate product...!"),
	MASTER_CON_02("MASTER_CON_02", "Pawn price not compliance with the rule...! "),
	MASTER_CON_03("MASTER_CON_03", "Interest rate not compliance with the rule...! "),

	
	PAYMENT_01("P_01", "Can't cancel installment status not paid...!"),
	PAYMENT_02("P_02", "Can't do partial payment...!"),
	
	JOURNAL_01("JP_01", "Journal entry must add chart account...!"),
	JOURNAL_02("JP_02", "Item of debit must equal credit...!"),
	
	;
	
	private String code;
	private String message;

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
