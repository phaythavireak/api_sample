package com.example.demo.controller;

import com.example.demo.exception.BusinessException;
import com.example.demo.http.ResponseMessage;
import com.example.demo.http.ResponseMessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class TestController {
    @GetMapping("/test")
    public ResponseMessage<Map> getStatusMorakot() throws BusinessException {
        Map<String, Object> map = new HashMap<>();
        List<String> status = Arrays.asList("sds","sdsd");
        if (status.size() > 0) {
            map.put("content", status);
        }else {
            throw new BusinessException("errors");
        }
        return ResponseMessageBuilder.instance(map)
                .build();
    }


}
