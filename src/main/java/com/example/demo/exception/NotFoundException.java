package com.example.demo.exception;

import com.example.demo.constants.Codes;

public class NotFoundException extends RuntimeException {
    private String code;

    public NotFoundException(Codes code) {
        super(code.getMessage());
        this.code = code.getCode();
    }

    public NotFoundException(String fieldName, Object value) {
        this(String.format("%s [%s] is could not found in system ", fieldName, value.toString()));
    }
    public NotFoundException(String fieldName, String value) {
        this(String.format("%s [%s] is could not found in system ", fieldName, value));
    }

    public NotFoundException(String message) {
        super(message);
    }
}
