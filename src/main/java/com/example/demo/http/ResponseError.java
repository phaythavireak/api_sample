package com.example.demo.http;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents Response Error
 *
 *
 */
@Data
@AllArgsConstructor
public class ResponseError {
	
	/*@ApiModelProperty(required = true)
	private final String code;*/

	private final String message;


}
