package com.example.demo.http;

import lombok.Data;


/**
 * Represent Response Message format
 *
 *
 * @param <T>
 */
@Data
public class ResponseMessage <T> {

	private ResponseStatuses status;
	/*
	@ApiModelProperty(required = true)
	private List<ResponseError> errors;*/

	private T errors;

	private T data;
}
