package com.example.demo.http;

import java.util.HashMap;
import java.util.Map;


/**
 *
 *
 */
public class ResponseMessageBuilder {
	
	private ResponseMessage<Object> message = new ResponseMessage<Object>();
	
	/**
	 * Create an instance of ResponseMessageBuilder
	 * 
	 * <br/>
	 * @param isSuccess 
	 * 		<br/>true : create default success Response
	 * 		<br/>else : create default error Response
	 * 		
	 * @return
	 */
	public static ResponseMessageBuilder instance(boolean isSuccess) {
		if(isSuccess) {
			return success();
		}
		else {
			return fail();
		}
	}
	
	
	/**
	 * Create an instance of ResponseMessageBuilder
	 * 
	 * <br/>
	 * @param data 
	 * 		<br/>NULL : create default error Response
	 * 		<br/>Otherwise : create success Response with passing data
	 * 		
	 * @return
	 */
	public static ResponseMessageBuilder instance(Object data) {
		if(data != null) {
			if(data.getClass().isAssignableFrom(Boolean.class)) {
				return instance((boolean)data);
			}
			else {
				ResponseMessageBuilder success = success();
				success.addData(data);
				return success;
			}
			
		}
		else {
			ResponseMessageBuilder fail = fail();
			return fail;
		}
	}
	
	
	/**
	 * Create default success ResponseMessage
	 * <br/>
	 * ResultCode: 0000
	 * ResultMessage: success
	 * @return
	 */
	public static ResponseMessageBuilder success() {
		ResponseMessageBuilder builder = new ResponseMessageBuilder();
		builder.message.setStatus(ResponseStatuses.SUCCESS);
		return builder;
	}
	
	/**
	 * Create default fail ResponseMessage
	 * <br/>
	 * ResultCode: SYS001
	 * ResultMessage: System Error
	 * @return
	 */
	public static ResponseMessageBuilder fail() {
		ResponseMessageBuilder builder = new ResponseMessageBuilder();
		builder.message.setStatus(ResponseStatuses.FAIL);
		
		return builder;
	}
	
	
	
	/**
	 * Add Error Code
	 * @param code
	 * @return
	 */
	public ResponseMessageBuilder addCode(String message) {
		ResponseError responseError = (ResponseError) this.message.getErrors();
		if(responseError == null) {
			responseError = new ResponseError(message);
		}
		this.message.setErrors(responseError);
		return this;
	}

	public ResponseMessageBuilder addCode(String field,String message) {
        Map resultCodes = createOrGetResultCodes();
		//this.message.setTest(test);
		resultCodes.put(field,new ResponseError(message));
		return this;
	}
	
	/**
	 * Internally create or get ResultCodes from ResponseMessage
	 * @return
	 */
	/*private List<ResponseError> createOrGetResultCodes() {
		//List<ResponseError> resultCodes = this.message.getErrors();
		MultiValueMap<String,ResponseError> resultCodes = this.message.getErrors();
		
		if(resultCodes == null) {
			resultCodes = new LinkedMultiValueMap<>();
			this.message.setErrors(resultCodes);
		}
		
		return resultCodes;
	}*/


	private Map<String ,ResponseError> createOrGetResultCodes() {
		//List<ResponseError> resultCodes = this.message.getErrors();
		Map<String,ResponseError> resultCodes = (Map<String, ResponseError>) this.message.getErrors();

		if(resultCodes == null) {
			resultCodes = new HashMap<String,ResponseError>();
			this.message.setErrors(resultCodes);
		}

		return resultCodes;
	}
	
	/**
	 * Add Response Data
	 * @param data
	 * @return
	 */
	public ResponseMessageBuilder addData(Object data) {
		this.message.setData(data);
		return this;
	}
	
	
	/**
	 * Finally call to construct ResponseMessage
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public final <T> ResponseMessage<T> build() {
		return (ResponseMessage<T>) message;
	}
}
