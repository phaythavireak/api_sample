package com.example.demo.http;

/**
 * Resent Response status i.e SUCCESS or FAIL
 *
 *
 */
public enum ResponseStatuses {
	SUCCESS,
	FAIL
}
