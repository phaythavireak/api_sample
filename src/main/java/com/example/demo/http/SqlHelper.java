package com.example.demo.http;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.lang.reflect.Field;
import java.util.*;

public class SqlHelper {

    public static Map prepareInsertQuery(Object obj) {

        Map<String, Object> mapValue = new HashMap<>();
        Class<?> kls = obj.getClass();
        Field[] tt = kls.getDeclaredFields();
        for (Field field : tt) {
            if (field.isAnnotationPresent(JsonProperty.class)) {
                try {
                    String annotationValue = field.getAnnotation(JsonProperty.class).value();
                    mapValue.put(annotationValue, field.get(obj));
                 /*   objJson.put(annotationValue, field.getName());
                    field.get(mktCustomerDto);*/

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        return mapValue;
    }


    public static String buildInsertQuery(String tableName, Map<String, Object> parameters) {
        List<Object> queryParameterValues = new ArrayList<>();
        StringBuilder columnString = new StringBuilder(" (");
        StringBuilder valueString = new StringBuilder(" VALUES (");
        boolean flag = false;
        for (Map.Entry param : parameters.entrySet()) {
            if (flag) {
                columnString.append(",");
                valueString.append(",");
            }
            flag = true;
            columnString.append(param.getKey());
            valueString.append("?");
            queryParameterValues.add(param.getValue());
        }
        columnString.append(") ");
        valueString.append(") ");
       /* insertClause = tableName + columnString.toString() + valueString.toString();
        mode = Mode.INSERT;*/
        return tableName + columnString.toString() + valueString.toString();
    }

    public static Object[] buildInsertValie(Object obj) {
        Map<String, Object> mapValue = new HashMap<>();
        Object[] values = null;
        Class<?> kls = obj.getClass();
        Field[] tt = kls.getDeclaredFields();
        boolean flag = false;
        List<Object> listValues = new ArrayList<>();
        try {
            for (Field field : tt) {
                listValues.add(field.get(obj));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        values = listValues.toArray(new Objects[listValues.size()]);
        return values;

    }

}
