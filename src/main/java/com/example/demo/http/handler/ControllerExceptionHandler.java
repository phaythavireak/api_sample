package com.example.demo.http.handler;

import com.example.demo.exception.BusinessException;
import com.example.demo.http.ResponseMessage;
import com.example.demo.http.ResponseMessageBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;


/**
 * Handler to handle system exception and return as Json
 * <br/>
 * Handle with all controllers
 *
 *
 */
@RestControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Handle all system Exception
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseMessage<Object> handleAllException(Exception ex) {

        String message = ex.getMessage();
        if (StringUtils.isEmpty(message)) {
            System.out.println(ex);
        } else {
            System.out.println(message + ex);
        }
		/*return ResponseMessageBuilder.fail()
			.addCode(Codes.ERROR.getCode(), Codes.ERROR.getMessage())
			.build();*/
        return ResponseMessageBuilder.fail()
                .addCode(ex.getMessage())
                .build();
    }

    @ExceptionHandler(InsufficientAuthenticationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseMessage<Object> handleAllException(InsufficientAuthenticationException ex) {
        String message = ex.getMessage();
        if (StringUtils.isEmpty(message)) {
            System.out.println(ex);
        } else {
            System.out.println(message + ex);
        }
        return ResponseMessageBuilder.fail()
                .addCode(ex.getMessage())
                .build();
    }

    /**
     * Handle all business exception Exception
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ResponseMessage<Object> handleBusinessException(BusinessException ex) {
        return ResponseMessageBuilder.fail()
                .addCode(ex.getMessage())
                .build();
    }

	/*@ExceptionHandler(AlreadyExistException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	public ResponseMessage<Object> handlAlreadyExistException(AlreadyExistException ex) {
		return ResponseMessageBuilder.fail()
				.addCode(ex.getMessage())
				.build();
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseMessage<Object> handlNotFoundException(NotFoundException ex) {
		return ResponseMessageBuilder.fail()
				.addCode(ex.getMessage())
				.build();
	}*/


    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseMessage<Object> handleValidationException(MethodArgumentNotValidException ex) {
        List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
        return buildResponseForValidationErrors(allErrors);
    }


    /**
     * Handle Validation exception
     * <br/>
     * Handle data validation on {@link Validated}
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseMessage<Object> handleValidationException(BindException ex) {
        List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
        return buildResponseForValidationErrors(allErrors);
    }

    /**
     * Internally build response message for errors from validation
     *
     * @param allErrors
     * @return
     */
    private ResponseMessage<Object> buildResponseForValidationErrors(List<ObjectError> allErrors) {
        ResponseMessageBuilder builder = ResponseMessageBuilder.fail();
        MultiValueMap test = new LinkedMultiValueMap();
        for (ObjectError objectError : allErrors) {
            if (objectError instanceof FieldError) {

                FieldError error = (FieldError) objectError;
                builder.addCode(error.getField(), String.format("Field %s %s", error.getField(), error.getDefaultMessage()));

				/*builder.addCode(HttpStatus.BAD_REQUEST.toString(),
						String.format("[%s]: %s", error.getField(), error.getDefaultMessage()));*/

            } else {
                builder.addCode(String.format("[%s]: %s", objectError.getObjectName(), objectError.getDefaultMessage()));
            }
        }


        return builder.build();
    }

}
