package com.example.demo.http.handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;


public class HttpStatusHandlerException {

    private HttpStatus httpStatus;

    private HttpStatusCodeException ex;

    public HttpStatusHandlerException(HttpStatusCodeException e) {
        ex = e;
    }

    public String getMktErrorMessage() {
        ObjectMapper mapper = new ObjectMapper();
        String errMessage = "";
        try {
            JsonNode errorMessage = mapper.readTree(ex.getResponseBodyAsString());
            if (errorMessage.get(RouteApi.MESSAGE) != null) {
                errMessage = errMessage + errorMessage.get(RouteApi.MESSAGE).toString();
            }

        } catch (IOException ef) {
            System.out.println("Error during parsing HttpStatusCodeException " + ef.getMessage());
        }
        return errMessage;
    }

    public String getHttpCodeErrorMgs() {
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            JsonNode errorMessage = mapper.readTree(ex.getResponseBodyAsString());
            if (errorMessage.get(RouteApi.ERRORS) != null) {
                JsonNode errJson = mapper.readTree(errorMessage.get("errors").toString());
                if (errJson.get(RouteApi.MESSAGE) != null) {
                    result = result + errJson.get(RouteApi.MESSAGE).toString();
                }
            }

        } catch (IOException ef) {
            System.out.println("Error during parsing HttpStatusCodeException " + ef.getMessage());
        }
        return result;

    }


}
