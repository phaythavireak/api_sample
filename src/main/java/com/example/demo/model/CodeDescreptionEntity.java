package com.example.demo.model;

import com.example.demo.model.share.BaseEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
public class CodeDescreptionEntity extends BaseEntity {

    @JsonProperty("code")
    @Column(name = "code",length = 50,unique=true)
    protected String code;

    @Column(name = "description",length = 5000)
    protected String description;
    
    @Column(name = "description_en",length = 5000)
    protected String descriptionEn;

/*  @Transient
    public String getTranslateDescription() {
        String result = getDescriptionEn();
        String resultKh = getDescription();
        if (!ProfileUtils.isLocalEnglish() && resultKh != null && !resultKh.isEmpty()) {
            result = resultKh;
        }
        return result;
    }*/
}
