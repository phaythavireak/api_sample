package com.example.demo.model.security;

import com.example.demo.model.share.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.*;


@Setter
@Getter
@Entity
@Table(name = "t_user")
@JsonIgnoreProperties({"password", "passwordQuestion1", "passwordAnswer1", "passwordQuestion2",
        "passwordAnswer2", "profiles", "accountNonExpired", "accountNonLocked", "credentialsNonExpired","userBranch"})
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity implements UserDetails {

    private static final long serialVersionUID = 6355365764819017081L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotEmpty
    @Column(name = "usr_first_name", length = 100)
    private String firstName;
    @NotEmpty
    @Column(name = "usr_last_name", length = 100)
    private String lastName;
    @NotEmpty
    @Column(name = "usr_pwd", nullable = false)
    @NotEmpty
    private String password;
    @Column(name = "usr_login", unique = true, nullable = false)
    @NotEmpty
    private String username;


    private String name;
    @Version
    private Long version =0l;
    private Integer level;

    @Column(name = "usr_expiration_date")
    private Date expirationDate;



    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "table_user_profile",
            joinColumns = {@JoinColumn(name = "usr_id")},
            inverseJoinColumns = {@JoinColumn(name = "pro_id")})
    private List<UserProfile> profiles;


    @Column(name = "usr_status")
    private Boolean active;

    @Column(name = "usr_phone", length = 15)
    private String phone;

    @Column(name = "list_ip_address", length = 1000)
    private String listIpAddress;

    private transient Set<GrantedAuthority> authorities = new HashSet<>();
    @Transient
    private UserProfile profile;


    @Transient
    public UserProfile getProfile() {
        if (profiles != null && profiles.size() > 0) {
            profile = profiles.get(0);
        }
        return profile;
    }
    @Transient
    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

/*  @JsonIgnore
    @ManyToMany(mappedBy = "users")
    private Set<MenuPage> menuPages;*/


    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (profiles != null) {
            for (UserProfile profile : profiles) {
                ProfileGrantedAuthority auth = new ProfileGrantedAuthority();
                auth.setProfile(profile);
                if (authorities == null) {
                    authorities = new HashSet<>();
                }
                authorities.add(auth);

            }
            if (profiles.size() > 0) {
                profile = profiles.get(0);
            }
        }
        return authorities;
    }


    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return active;
    }

    public void addProfile(UserProfile profile) {
        if (profiles == null) {
            profiles = new ArrayList<>();
        }
        profiles.add(profile);
    }

}
