package com.example.demo.model.security;

import org.springframework.security.core.GrantedAuthority;


public class ProfileGrantedAuthority implements GrantedAuthority {
    private static final String SPRING_PREF_ROLE = "ROLE_";

    private UserProfile profile = null;

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    @Override
    public String getAuthority() {
        if (profile == null) {
            return null;
        }
        if (profile.getCode().toLowerCase().startsWith(SPRING_PREF_ROLE)) {
            return profile.getCode();
        }
        return SPRING_PREF_ROLE + profile.getCode();
    }

    @Override
    public int hashCode() {
        return profile.getCode().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ((ProfileGrantedAuthority) obj).profile.getCode().equals(this.profile.getCode());
    }
}
