package com.example.demo.model.security;


import com.example.demo.model.CodeDescreptionEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Data
@Entity
@Table(name = "t_profile")
@EqualsAndHashCode(callSuper = false)
public class UserProfile extends CodeDescreptionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;


    @Override
    public String toString() {
        return "Person [id=" + id + ", code=" + code + ", descriptionEn=" + descriptionEn + ", description=" + description + "]";
    }

}
