package com.example.demo.model.share;

import com.example.demo.share.DateUtils;
import com.example.demo.share.SecurityUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.nashorn.internal.objects.annotations.Getter;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
/*@JsonIgnoreProperties(
        value = {"createdAt", "updatedAt","modifiedBy","modfiedAt","statusRecord","inactivatedDate","inactivatedBy"}
)*/
public class BaseEntity extends BaseAuditedEntity {

    /*    //@NotNull(message = "{fieldValidation.notNull}")
        @Column(name = "status_record", length = 10)
        @Enumerated(EnumType.STRING)
        @JsonIgnore
        private StatusRecord statusRecord;*/

    @Column(name = "is_activate")
    private Boolean isActivate​​;


    @JsonIgnore
    @Column(name = "inactivated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inactivatedDate;

    @JsonIgnore
    @Column(name = "inactivated_by", length = 30)
    private String inactivatedBy;


   /* @JsonIgnore
    @Version
    @Column(name = "version")
    private Long version;*/

    /*@Version
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "VERSION")
    private Date version;*/

    @PrePersist
    void prePersist() {
        if (isActivate​​ == null)
            isActivate​​ = true;

        if (getCreatedBy() == null) {
            String user = SecurityUtils.getCurrentUserLogin().orElse("system");
            setCreatedBy(user);
            setModifiedBy(user);
        }

        if (getCreatedAt() == null) {
            Date date = DateUtils.getCurrentDateTime();
            setCreatedAt(date);
            setModifiedAt(date);
        }
    }

    @PreRemove
    void preDelete() {
    }

    public Boolean getActivate​​() {
        return isActivate​​;
    }

    public void setActivate​​(Boolean activate​​) {
        isActivate​​ = activate​​;
    }

    public Date getInactivatedDate() {
        return inactivatedDate;
    }

    public void setInactivatedDate(Date inactivatedDate) {
        this.inactivatedDate = inactivatedDate;
    }

    public String getInactivatedBy() {
        return inactivatedBy;
    }

    public void setInactivatedBy(String inactivatedBy) {
        this.inactivatedBy = inactivatedBy;
    }
}
