package com.example.demo.service;

import com.example.demo.exception.BusinessException;

import com.example.demo.model.security.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by seang.kimsuor on 12/15/2017.
 */
public interface AuthenticationService extends UserDetailsService {

    User createUser(User user, String pwd) throws BusinessException;
    User updateUserPassword(String userName, String rawPwd);
    boolean checkUserByUsername(String username);
    User getUserByLogin(String userLogin);
    public User authenticateNormal(String user, String password);

    public void clearFailedLoginAttempts(User user);

    public void incrementFailedLoginAttempts(String username);

    public void authenticationSuccess(User user);

    public boolean verifyPassword(String password, String oldPassword) throws Exception;

    public boolean changePassword(User user) throws Exception;

}
