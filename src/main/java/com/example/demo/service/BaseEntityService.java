package com.example.demo.service;


import com.example.demo.utils.QueryResult;
import com.example.demo.utils.jpa.JoinCriteria;
import com.example.demo.utils.jpa.SearchCriteria;
import com.example.demo.utils.jpa.SpecificSearchCriteria;
import com.example.demo.vo.PageableRequestVO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

public interface BaseEntityService {

    SessionFactory getSessionFactory();

    Session getCurrentSession();
    <T> void saveOrUpdate(T entity);
    <T> T getFirstEntityByProperty(String property, Object value, Class<T> clazz);
    <T> boolean isAlreadyExistByProperty(String property, Object value, Class<T> clazz);

    <T> List<T> listEntitiesByProperty(String property, Object value, Class<T> clazz);
    <T> T getEntityById(Serializable id, Class<T> clazz);
    <T> void delete(Serializable id, Class<T> clazz);

    <T> void delete(T entity);
    <T> void inActiveEntity(Long id,Class<T> clazz);
    void flush();

    void clear();

    <T> List<T> fetchAll(Class<T> clazz);
    <T> List<T> fetchAllActive(Class<T> clazz);
    <T> List<T> findAllEntitiesOrderedBy(Class<T> entityClass, String orderByColumn, boolean ascending);
    <T> List<T> searchByCriteria(Class<T> clazz, List<SearchCriteria> searchCriteriaList, List<SearchCriteria> searchORByCriteria);
    <T> List<T> searchByCriteria(Class<T> clazz, SpecificSearchCriteria specificSearchCriteria);
    <T> List<T> searchByCriteria(Class<T> clazz, List<SearchCriteria> searchCriteriaList, List<SearchCriteria> searchORCriteriaList, List<JoinCriteria> joinCriteriaList, PageableRequestVO pageableRequestVO);
    
    <T> List<T> searchByCriteriaOR(Class<T> clazz, List<SearchCriteria> searchCriteriaList,List<SearchCriteria> searchORByCriteria);
    //<T> List<T> searchByCriteria(Class<T> clazz, List<SearchCriteria> searchCriteriaList,List<JoinCriteria> joinCriteriaList);
    <T> List<T> fetchActiveEntityByUserBranch(Class<T> clazz);
    //<T> List<T> fetchActiveEntityByBranch(Class<T> clazz, Branch branch);
    <T> Page<T> filter(Class<T> clazz, SpecificSearchCriteria specificSearchCriteria);
    <T> Page<T>  filter(Class<T> clazz, List<SearchCriteria> searchCriteriaList,List<SearchCriteria> searchORCriteriaList, List<JoinCriteria> joinCriteriaList, PageableRequestVO pageableRequestVO);
    <T> Page<T> filterActive(Class<T> clazz,String orderByColumn, boolean ascending);
    <T> Page<T> filterActive(Class<T> clazz, PageableRequestVO requestVO);
    <T> Page<T> filterActive(Class<T> clazz);
    
    QueryResult query(String query, String pagination, String countQuery);
    void save(String query);
}
