package com.example.demo.service;

import com.example.demo.model.security.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class UserAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {


    /**
     * A Spring Security UserDetailsService implementation based upon the Account entity model.
     */
    @Autowired
    private transient AuthenticationService userDetailsService;

    /**
     * A PasswordEncoder instance to hash clear test password values.
     */
    @Autowired
    private transient PasswordEncoder passwordEncoder;

    @Override
    protected void additionalAuthenticationChecks(final UserDetails userDetails,
                                                  final UsernamePasswordAuthenticationToken token) throws AuthenticationException {
        log.info("> additionalAuthenticationChecks");

        if (token.getCredentials() == null || userDetails.getPassword() == null) {
            log.info("< additionalAuthenticationChecks");
            // Count Login false
            //userDetailsService.incrementFailedLoginAttempts(userDetails.getUsername());
            throw new BadCredentialsException("Credentials may not be null.");
        }

        if (!passwordEncoder.matches((String) token.getCredentials(), userDetails.getPassword())) {
            log.info("< additionalAuthenticationChecks");
            // Count Login false
            //userDetailsService.incrementFailedLoginAttempts(userDetails.getUsername());
            throw new BadCredentialsException("Invalid credentials.");
        }

        log.info("< additionalAuthenticationChecks");
    }

    // this method is call during validation login
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
/*        WebAuthenticationDetails details = (WebAuthenticationDetails) authentication.getDetails();
        String userIp = details.getRemoteAddress();*/

        Authentication result = super.authenticate(authentication);

        if(result.isAuthenticated()) {
            User user = (User) result.getPrincipal();
            userDetailsService.authenticationSuccess(user);
            log.debug("Authenticated is successful");
        } else {
            log.debug("Authenticated is not successful");
        }
        return result;
    }

    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken token)
            throws AuthenticationException {
        log.info("> retrieveUser");
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        log.info("< retrieveUser");
        return userDetails;
    }

}
