package com.example.demo.service.impl;


import com.example.demo.constants.Codes;
import com.example.demo.exception.BusinessException;

import com.example.demo.exception.NotFoundException;
import com.example.demo.model.security.User;
import com.example.demo.service.AuthenticationService;
import com.example.demo.service.GenericEntityService;
import com.example.demo.utils.QueryUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    GenericEntityService genericEntityService;

    @Override
    public User createUser(User user, String pwd) throws BusinessException {
        if (user == null) {
            throw new RuntimeException("User  is null");
        }
        if (user.getId() != null && user.getId() > 0) {
            throw new RuntimeException("User [" + user.getUsername() + "] - Should have an empty id.");
        }
        if (StringUtils.isEmpty(pwd)) {
            throw new RuntimeException("User [" + user.getUsername() + "] - An empty password is not allowed");
        }
       if (user.getProfile() == null) {
            throw new BusinessException(Codes.ERROR_INVALID_USER_REQUEST.getCode());
        }
        if (checkUserByUsername(user.getUsername())) {
            throw new BusinessException(Codes.ERROR_DUPLICATE_USER_LOGIN.getMessage());
        }

        try {
            String encodedPwd = new BCryptPasswordEncoder().encode(pwd);

            //String encodedPwd = PasswordEncoder.encode(pwd);
            user.setPassword(encodedPwd);
            //userDao.save(user);
            genericEntityService.saveOrUpdate(user);
            log.info("The creation of the user ["
                    + user.getId() + "] ]"
                    + user.getUsername() + "]");
            // + user.getUserDes() +  "] is successful.");
            return user;
        } catch (Exception e) {
            String errMsg = "Error during the creation of the user [" + user.getUsername() + "]";
            log.error(errMsg);
            throw new RuntimeException(errMsg, e);
        }
    }
    

    @Override
    public User updateUserPassword(String userName, String rawPwd) {
        User user = getUserByLogin(userName);
        Assert.notNull(user, "User not found !");
        try {
            String encodedPwd = new BCryptPasswordEncoder().encode(rawPwd);
            //String encodedPwd =  PasswordEncoder.encode(rawPwd);
            user.setPassword(encodedPwd);
            genericEntityService.saveOrUpdate(user);
            log.info("The update user ["
                    + user.getId() + "] ]"
                    + user.getUsername() + "] ");
            //+ user.getUserDes() +  "] is successful.");
            
         	String query = QueryUtils.deleteToken(userName);
    		try {
    			genericEntityService.save(query);
    		} catch (Exception e) {
    			throw new BusinessException(Codes.ERROR.getMessage());
    		}
            
        } catch (Exception e) {
            String errMsg = "Error during the creation of the user [" + user.getUsername() + "]";
            log.error(errMsg);
            throw new RuntimeException(errMsg, e);
        }
        return user;
    }

    @Override
    public boolean checkUserByUsername(String username) {
        User user = getUserByLogin(username);
        return user != null;
    }

    @Override
    public User getUserByLogin(String userLogin) {
        User user = genericEntityService.getFirstEntityByProperty("username", userLogin, User.class);
        return user;
    }
    @Override
    public User authenticateNormal(String user, String password) {
        return null;
    }

    @Override
    public void clearFailedLoginAttempts(User user) {
        log.debug("Increment Failed Login");
    }

    @Override
    public void incrementFailedLoginAttempts(String username) {
        log.debug("Increment Failed Login");
    }

    @Override
    public void authenticationSuccess(User user) {

        log.debug("Authentication Success Login");
    }

    @Override
    public boolean verifyPassword(String password, String oldPassword) throws Exception {
        return false;
    }

    @Override
    public boolean changePassword(User user) throws Exception {
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = null;
        try {
            user = genericEntityService.getFirstEntityByProperty("username", username, User.class);
        } catch (Exception e) {
            throw new NotFoundException("The user " + username + "] can not be found.");
        }

        if (user == null) {
            throw new NotFoundException("The user " + username + " can not be found.");
        }
        final List<GrantedAuthority> authorities = new ArrayList(user.getAuthorities());
        if (authorities.size() == 0) {
            throw new InsufficientAuthenticationException("NO_ROLE_AVAILABLE");
        }
        if (authorities.size() > 0) {
            boolean anonymousOnly = true;
            for (GrantedAuthority grantedAuthority : authorities) {
                anonymousOnly = grantedAuthority.getAuthority().toLowerCase().contains("anonymous");
            }
            if (anonymousOnly) {
                throw new InsufficientAuthenticationException("ONLY_ANONYMOUS_ROLE_IS_FOUND");
            }
        }
        return user;
    }


}
