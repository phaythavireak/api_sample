package com.example.demo.share;

/**
 * Created by Kimsuor SEANG on 11/20/2017.
 */
public enum EnumDateRange {
    TODAY, YESTERDAY,  LAST_WEEK, CURRENT_WEEK, LAST_MONTH, CURRENT_MONTH, LAST_YEAR, CURRENT_YEAR
}