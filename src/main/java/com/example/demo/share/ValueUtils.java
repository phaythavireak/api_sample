package com.example.demo.share;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ValueUtils {

    public static String CURRENCY_PREFIX = "$";
    public static int DEFAULT_DECIMAL = 2;

    public static String getString(String value) {
        if (value != null) {
            return value;
        }
        return "";
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(String value, String defaultValue) {
        if (value != null) {
            return value;
        }
        return defaultValue;
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Double value) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Long value) {
        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return 0
     */
    public static String getStringWithZero(Double value) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return "0";
        }
    }

    public static String getStringWithZeroFormat(Double value) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return "0.0";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getStringWithDefaultVal(Double value, String val) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return val;
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Integer value) {

        if (value != null && value != 0) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static String getStringWithZero(Integer value) {

        if (value != null && value != 0 && value > 0) {
            return value.toString();
        } else {
            return "0";
        }
    }

    public static String getStringWithDouble(Double value) {

        if (value != null && value > 0d) {
            return value.toString();
        } else {
            return "0.00";
        }
    }

    /**
     * @param value
     * @return
     */
    public static String getStringWithNA(String value) {

        if (value != null && !value.trim().isEmpty()) {
            return value;
        } else {
            return "N/A";
        }
    }

    public static String getInteger(Integer value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static String getDoubles(Double value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return
     */
    public static Long getLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (Exception ex) {
            return 0L;
        }
    }


    public static String getStrings(Integer value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Float value) {

        if (value == 0f || value == 0d || value == null) {
            return "";
        } else {
            return value.toString();
        }
    }

    /**
     * @param value
     * @return double
     */
    public static Double getDouble(String value) {
        try {
            return !Double.isNaN(Double.parseDouble(value)) ? Double.parseDouble(value) : 0d;
        } catch (NumberFormatException | NullPointerException nfe) {
            return 0d;
        }
    }

    /**
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(String value) {
        try {
            if (!isNullOrEmpty(value)) {
                return new BigDecimal(value);
            }
            return BigDecimal.valueOf(0);
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0);
        }
    }

    /**
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(Double value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0);
        }
    }

    /**
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(Long value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0);
        }
    }

    public static String getBigDecimalDouble(Double value) {
        try {
            return new BigDecimal(value).setScale(2, BigDecimal.ROUND_FLOOR).toString();
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0).toString();
        }
    }

    public static String getBigDecimalInteger(Integer value) {
        try {
            return new BigDecimal(value).setScale(2, BigDecimal.ROUND_FLOOR).toString();
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0).toString();
        }
    }

    /**
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(Integer value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0);
        }
    }

    /**
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(Float value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException | NullPointerException nfe) {
            return BigDecimal.valueOf(0);
        }
    }


    /**
     * @param value
     * @return double
     */
    public static Double getDouble(Double value) {
        if (value != null) {
            return getDouble(getString(value));
        } else {
            return 0d;
        }
    }

    /**
     * @param value
     * @return double
     */
    public static Double getDouble(Integer value) {
        try {
            return getDouble(value.toString());
        } catch (Exception ex) {
            return 0d;
        }
    }

    /**
     * @param value
     * @return float
     */
    public static Float getFloat(String value) {

        if (value != null && !value.isEmpty()) {
            return Float.parseFloat(value);
        } else {
            return 0f;
        }
    }

    /**
     * @param value
     * @return
     */
    public static Integer getInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static double getHoursKaraoke(Long minutes, int splitTime) {
        double hour = 0l;
        if (minutes != null) {
            if (minutes >= splitTime && minutes < 30 + splitTime) {
                hour = 0.5;
            } else if (minutes >= 30 + splitTime) {
                hour = 1d;
            }
        }
        return hour;
    }

    public static String getTime(Long millis) {
        if (millis != null) {
            String s = "" + (millis / 1000) % 60;
            String m = "" + (millis / (1000 * 60)) % 60;
            String h = "" + (millis / (1000 * 60 * 60)) % 24;
            String d = "" + (millis / (1000 * 60 * 60 * 24));
            return d + "d " + h + "h:" + m + "m:" + s + "s";
        }
        return "N/A";
    }


    private static Long getProcessTime(Date start) {
        long times = 0L;
        Date end = DateUtils.today();
        times = end.getTime() - start.getTime();
        return times;
    }

    public static Integer getInt(Double value) {

        if (value != null) {
            return value.intValue();
        } else {
            return 0;
        }
    }

    public static Integer getInt(int value) {

        if (value > 0) {
            return value;
        } else {
            return 0;
        }
    }

    public static String getStringFormat(String value) {
        String strValue = "";
        if (value != null) {
            try {
                Double dobValue = Double.parseDouble(value);
                strValue = dobValue.toString();
            } catch (Exception e) {
                strValue = value;
            }

        }
        return strValue;
    }

    public static String getStringFromArray(String... list) {
        List<String> valueList = new ArrayList<>();
        for (String value : list) {
            if (!ValueUtils.isNullOrEmpty(value)) {
                valueList.add(value);
            }
        }
        return ValueUtils.getStringFromArray(valueList);
    }

    public static String getStringFromArray(List<String> list) {
        return list.stream().collect(Collectors.joining(","));
    }

    public static List<String> getArrayFromString(String listString) {
        if (listString != null) {
            List<String> list = Arrays.asList(listString.split(","));
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    list.set(i, list.get(i).trim());
                }
                return list;
            }
        }
        return Arrays.asList();
    }

    public static <T> List<T> getDeletedItems(List<T> oldItems, List<T> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return oldItems;
        } else if (oldItems != null) {
            return oldItems.stream().filter(item -> !newItems.contains(item)).collect(Collectors.toList());
        }
        return null;
    }

    public static boolean isNullOrEmpty(String object) {
        if (object == null || object.isEmpty()) {
            return true;
        }
        return false;
    }

    public static double round(double value, int places) {
        if (value == 0) return 0;
        if (places < 0) {
            places = DEFAULT_DECIMAL;
        }
        value = getDouble(value);
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static String formatDecimal(Double value, int dec) {
        value = getDouble(value);
        value = round(value, dec);
        return String.format("%." + dec + "f", value);
    }

    public static Double getDoubleFormat(Double value, int dec) {
        if (value == null) {
            value = 0D;
        }
        value = round(value, dec);
        return getDouble(String.format("%." + dec + "f", value));
    }

    public static String getDoubleFormatPercentage(Double value, int dec) {
        value = getDouble(value);
        value = round(value, dec);
        return String.format("%." + dec + "f", value) + "%";
    }

    public static String getCurrencyNoDollar(Double value) {
        if (value != null && value != 0D) {
            String format = new DecimalFormat("#,###").format(value);
            return format;
        }
        return "0";
    }

    public static String getCurrency(Double value) {
        if (value != null && value != 0D) {
            String format = new DecimalFormat("#,###.00").format(value);
            return format;
        }
        return "0.00";
    }

    public static String getDoubleNoFormat(Double value) {
        if (value != null && value != 0D) {
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(value);
            return formatted;
        }
        return "0";
    }

    public static String getDoubleFormat(Double value) {
        if (value != null && value != 0D) {
            DecimalFormat df = new DecimalFormat("#.##");
            String formatted = df.format(value);
            return formatted;
        }
        return "0.0";
    }

    public static String getDoubleNoFormats(Double value) {
        if (value != null && value != 0D) {
            DecimalFormat df = new DecimalFormat("#");
            String formatted = df.format(value);
            return formatted;
        }
        return "0";
    }


    public static Double getDoubleFormat(String valueStr, int dec) {
        double value = round(getDouble(valueStr), dec);
        return getDouble(String.format("%." + dec + "f", value));
    }

    public static String formatDecimal(Double value) {
        return formatDecimal(value, DEFAULT_DECIMAL);
    }

    public static String formatCurrency(Double value) {
        return formatCurrency(value, DEFAULT_DECIMAL);
    }

    public static String formatCurrency(Double value, int dec) {
        return formatCurrency(value, null, dec);
    }

    public static String formatCurrency(Double value, String prefix, int dec) {
        if (prefix == null) {
            prefix = CURRENCY_PREFIX;
        }
        if (dec < 0) {
            int mul = getInt(String.format("1%0" + Math.abs(dec) + "d", 0));
            if (mul > 0) {
                value = value / mul;
                value = round(value, 0);
                value = value * mul;
            }
            dec = 1;
        }
        value = getDouble(value);
        value = round(value, dec);
        String defaultFormat = prefix + "###,###";
        switch (dec) {
            case 1:
                defaultFormat += ".#";
                break;
            default:
                defaultFormat += ".##";
        }
        DecimalFormat decimalFormat = new DecimalFormat(defaultFormat);
        return decimalFormat.format(value);
    }

    public static List<String> convertStringToList(String value) {
        if (StringUtils.isNotEmpty(value)) {
            List<String> stringList = Arrays.asList(value.split(","));
            return stringList.stream().map(item -> item.trim()).collect(Collectors.toList());
        }
        return null;
    }

    public static <T> T getObjectFromMap(Map<String, Object> mapData, String keyMap, Class<T> type) {
        if (StringUtils.isNotEmpty(keyMap) && mapData != null) {
            Object obj = mapData.get(keyMap);
            if (obj != null) {
                return ValueUtils.convertObjectToClass(obj, type);
            }
        }
        return null;
    }

    public static <T> T convertObjectToClass(Object obj, Class<T> type) {
        if (obj != null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                return mapper.readValue(mapper.writeValueAsString(obj), type);
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public static Boolean getBooleanValue(Boolean value) {
        if(value != null) {
            return value;
        } else  {
            return Boolean.FALSE;
        }
    }

    public static int getValidateKhmer(String regex, String value) {
        int index = 0;
        String lastNameKh = StringUtils.trim(value).replaceAll("\\s+", "");
        for (char c : lastNameKh.toCharArray()) {
            if (Pattern.matches(regex, String.valueOf(c))) {
                index++;
            }
        }
        return index;
    }


    public static <T> T convertValue(Object value, Class<T> type) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.convertValue(value, type);
        } catch (Exception ex) {
            return null;
        }
    }

}