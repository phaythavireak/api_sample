package com.example.demo.utils;

import com.example.demo.constants.BooleanTypes;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;
import org.springframework.util.Assert;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
public abstract class BeanUtils {
	
	static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
	static {
		ConverterFactory converterFactory = mapperFactory.getConverterFactory();
		converterFactory.registerConverter(new BooleanToStringBidirectionalConverter());
		converterFactory.registerConverter(new BooleanTypeToStringBidirectionalConverter());
		converterFactory.registerConverter(new DateToOffsetDateTimeBidirectionalConverter());
	}
	
	public static <T> T parse(Object bean, Class<T> clazz) {
		Assert.notNull(bean, "bean cannot be null");
		MapperFacade mapper = createMapper(bean.getClass(), clazz);
		
		try {
			return mapper.map(bean, clazz);
		} catch (Exception e) {
			log.error("Cannot parse bean into class: {}", clazz, e);
			return null;
		}
	}
	
	
	public static <S, D> List<D> parseList(List<S> list, Class<D> clazz) {
		Assert.notNull(list, "bean cannot be null");
		
		List<D> result = new ArrayList<>();
		for (Object object : list) {
			MapperFacade mapper = createMapper(object.getClass(), clazz);
			D map = mapper.map(object, clazz);
			result.add(map);
		}
		
		return result;
	}
	
	public static void copyNonNullProperties(Object source, Object target) {
		MapperFacade mapper = createMapper(source.getClass(), target.getClass());
		try {
			mapper.map(source, target);
		} catch (Exception e) {
			log.error("Cannot copy bean into: {}", target, e);
		}
	}
	
	
	private static <S, D> MapperFacade createMapper(Class<S> sourceClass, Class<D> destinationClass) {
		mapperFactory.classMap(sourceClass, destinationClass);
	    return mapperFactory.getMapperFacade();
	}
	
	
	private static class DateToOffsetDateTimeBidirectionalConverter extends BidirectionalConverter<Date, OffsetDateTime> {

		@Override
		public OffsetDateTime convertTo(Date source, Type<OffsetDateTime> destinationType,
				MappingContext mappingContext) {
			if(source != null) {
				return DateTimeUtils.toOffsetDateTime(source);
			}
			else {
				return null;
			}
		}

		@Override
		public Date convertFrom(OffsetDateTime source, Type<Date> destinationType, MappingContext mappingContext) {
			if(source != null) {
				return DateTimeUtils.toDate(source);
			}
			else {
				return null;
			}
		}
	}
	
	private static class BooleanToStringBidirectionalConverter extends BidirectionalConverter<Boolean, String> {
		
		@Override
		public String convertTo(Boolean source, Type<String> destinationType, MappingContext mappingContext) {
			
			if(source != null) {
				BooleanTypes instanceOf = BooleanTypes.instanceOf(source);
				return instanceOf.getValue();
			}
			else {
				return null;
			}
		}
		@Override
		public Boolean convertFrom(String source, Type<Boolean> destinationType, MappingContext mappingContext) {
			if(source != null) {
				BooleanTypes instanceOf = BooleanTypes.instanceOf(source);
				if(instanceOf == BooleanTypes.YES) {
					return Boolean.TRUE;
				}
				else {
					return Boolean.FALSE;
				}
			}
			else {
				return null;
			}
		}
	}
	
	private static class BooleanTypeToStringBidirectionalConverter extends BidirectionalConverter<BooleanTypes, String> {
		@Override
		public String convertTo(BooleanTypes source, Type<String> destinationType, MappingContext mappingContext) {
			if(source != null) {
				return source.getValue();
			}
			else {
				return null;
			}
		}
		@Override
		public BooleanTypes convertFrom(String source, Type<BooleanTypes> destinationType,
				MappingContext mappingContext) {
			return BooleanTypes.instanceOf(source);
		}
	}
}
