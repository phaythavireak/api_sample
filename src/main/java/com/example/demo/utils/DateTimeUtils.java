package com.example.demo.utils;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static org.joda.time.DateTimeFieldType.*;

/**
 * Utility on system date time
 *
 * @author
 */
public abstract class DateTimeUtils {

    public static final String FORMAT_YYYYMMDD = "yyyy-MM-dd";
    public static final String FORMAT_YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_YYYYMMDD_HHMM = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String FORMAT_YYYYMMDD_HHMMSS_NOSEP = "yyyyMMdd_HH:mm:ss";
    public static final String FORMAT_YYYYMMDD_NOSEP = "yyyyMMdd";
    public static final String FORMAT_DDMMYYYY_SLASH = "dd/MM/yyyy";
    public static final String FORMAT_DDMMYYYY_HHMM_SLASH = "dd/MM/yyyy HH:mm";
    public static final String FORMAT_STR_DDMMYY_NOSEP = "ddMMyy";
    public static final String FORMAT_STR_DDMMYYYY_NOSEP = "ddMMyyyy";
    public static final String FORMAT_DDMMYYYY_MINUS = "dd-MM-yyyy";
    public static final String FORMAT_DD_MM_YYYYY_HHMMSS = "dd-MM-yyyy hh:mm:ss";
    public static final String FORMAT_YYYY_MM_DD_SQL = "yyyy-mm-dd";
    public static final String FORMAT_YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT_HH_MM = "HH:mm a";
    public static final String FORMAT_HH_MM_SS = "hh:mm:ss";

    public static final String FORMAT_HH_MM_23 = "HH:mm";
    
    public static final String FORMAT_YYMMDD = "yyMMdd";
    
    public static String DEFAULT_DATE_FORMAT = "dd-MMM-yyyy";
    public static int calculateDays(Date date1, Date date2) {
        DateTime dateTime1 = new DateTime(date1);
        DateTime dateTime2 = new DateTime(date2);
        return Days.daysBetween(dateTime1, dateTime2).getDays();
    }

    public static long dateDiff(Date date1, Date date2) {
        // Get msec from each, and subtract.
        long diff = date2.getTime() - date1.getTime();
        return diff;
    }

    public static Date getCurrentDate() {
        Date currentDateTime = getCurrentDateTime();

        return getDateTime000000(currentDateTime);
    }

    public static Date getCurrentDateTime() {
        DateTime now = DateTime.now();
        return now.toDate();
    }

    public static OffsetDateTime getCurrentDateTimeAsOffsetDateTime() {
        return toOffsetDateTime(getCurrentDateTime());
    }

    public static OffsetDateTime toOffsetDateTime(Date date) {
        return OffsetDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static String getDateLabel(final Date date, final String formatPattern) {
        if (date != null && formatPattern != null) {
            return DateFormatUtils.format(date, formatPattern);
        }
        return null;
    }

    public static Date toDate(OffsetDateTime offsetDateTime) {
        return Date.from(offsetDateTime.toInstant());
    }

    public static Date getLastDayOfMonthDateTime235959(Date inputDate) {
        DateTime DateTime = new DateTime(inputDate);

        DateTime = DateTime.withDayOfMonth(DateTime.dayOfMonth().getMaximumValue()).millisOfDay().withMaximumValue();

        return DateTime.toDate();
    }

    public static Date getDateTime000000(Date date) {
        DateTime dateTime = new DateTime(date).withTimeAtStartOfDay();

        return dateTime.toDate();
    }

    public static Date getDateTime235959(Date date) {

        DateTime dateTime = new DateTime(date).millisOfDay().withMaximumValue();

        return dateTime.toDate();
    }

    public static boolean isNotPastDate(Date date) {
        Date dateOnly = DateTimeUtils.getDateTime000000(date);

        int calculateDays = DateTimeUtils.calculateDays(DateTimeUtils.getCurrentDate(), dateOnly);

        return calculateDays >= 0;
    }

    public static int getYear() {
        DateTime now = DateTime.now();
        return now.get(year());
    }

    public static int getMonth() {
        DateTime now = DateTime.now();
        return now.get(monthOfYear());
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getDay() {
        DateTime now = DateTime.now();
        return now.get(dayOfYear());
    }

    public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static Date addDay(Date date, int day) {
        DateTime dateTime = new DateTime(date);
        DateTime plusDays = dateTime.plusDays(day);
        return plusDays.toDate();
    }
    public static Date addWeek(Date date, int week) {
        DateTime dateTime = new DateTime(date);
        DateTime plusDays = dateTime.plusWeeks(week);
        return plusDays.toDate();
    }
    public static Date addMonth(Date date, int month) {
        DateTime dateTime = new DateTime(date);
        DateTime plusDays = dateTime.plusMonths(month);
        return plusDays.toDate();
    }
    public static Date addHour(Date date, int hour) {
        DateTime dateTime = new DateTime(date);
        DateTime plusDays = dateTime.plusHours(hour);
        return plusDays.toDate();
    }

    public static Date addMinutes(Date date, int mm) {
        DateTime dateTime = new DateTime(date);
        DateTime plusDays = dateTime.plusMinutes(mm);
        return plusDays.toDate();
    }

    public static OffsetDateTime getStartDateAsOffsetDateTime(Date date) {
        return toOffsetDateTime(DateTimeUtils.getDateTime000000(date));
    }

    public static OffsetDateTime getEndDateAsOffsetDateTime(Date date) {
        return toOffsetDateTime(DateTimeUtils.getDateTime235959(date));
    }

    public static Date getDateAtBeginningOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(6, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar.getTime();
    }

    public static Date getDateAtEndOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDateAtBeginningOfYear(date));
        calendar.add(1, 1);
        calendar.add(14, -1);
        return calendar.getTime();
    }

    public static LocalTime getLocalTime(Date date) {
        if (date != null) {
            Date input = date;
            return input.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        }
        return null;
    }

    public static LocalDate getLocalDate(Date date) {
        if (date != null) {
            Date input = date;
            return input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
        return null;
    }

    public static Date convertLocalDateToDate(LocalDate localDate) {
        if (localDate != null) {
            return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

    public static Date getDateAtBeginningOfDay(final Date date) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getDateAtBeginningOfDay(final LocalDate date) {
        return getDateAtBeginningOfDay(java.sql.Date.valueOf(date));
    }

    public static Date string2DateDDMMYYYY_SLASH(String dt) {
        try {
            return string2Date(dt, FORMAT_DDMMYYYY_SLASH);
        } catch (Exception e) {
            return string2Date(dt, FORMAT_DDMMYYYY_MINUS);
        }
    }

    public static Date string2DateYYYY_MM_DD_T_HH_MM_SS(String dt) {
        try {
            return string2Date(dt, FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        } catch (Exception e) {
            return string2Date(dt, FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        }
    }

    public static String date2StringYYYY_MM_DD_T_HH_MM_SS(Date dt) {
        return sampleDateToString(dt, FORMAT_YYYY_MM_DD_T_HH_MM_SS);
    }

    public static Date string2DateFORMAT_YYYYMMDD(String dt) {
        try {
            return string2Date(dt, FORMAT_YYYYMMDD);
        } catch (Exception e) {
            return string2Date(dt, FORMAT_YYYYMMDD);
        }
    }

    public static Date string2DateSql(String dt) {
        try {
            return string2Date(dt, FORMAT_YYYY_MM_DD_SQL);
        } catch (Exception e) {
            return string2Date(dt, FORMAT_YYYY_MM_DD_SQL);
        }
    }

    public static Date string2DateDDMMYYYYNoSeparator(String dt) {
        return string2Date(dt, FORMAT_STR_DDMMYYYY_NOSEP);
    }

    public static Date string2DateDDMMYYNoSeparator(String dt) {
        return string2Date(dt, FORMAT_STR_DDMMYYYY_NOSEP);
    }


    public static Date string2Date(String dt, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(dt);
        } catch (ParseException e) {
            return minDate();
        }
    }

    public static String sampleDateToString(Date dt, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.format(dt);
        } catch (Exception e) {
            return date2StringDB_YYYYMMDD(dt);
        }

    }

    public static Date minDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(1901, 01, 01);
        return cal.getTime();
    }

    public static Date getDateAtEndOfDay(final LocalDate date) {
        return getDateAtEndOfDay(java.sql.Date.valueOf(date));
    }

    public static Date getDateAtEndOfDay(final Date date) {
        if (date == null) {
            return null;
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDateAtBeginningOfDay(date));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }


    public static final String dateToString(Date dt, String tzString, String dateformat) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.setTimeZone(TimeZone.getTimeZone(tzString));
        return calendar2String(cal, dateformat);
    }

    public static final String date2String(Date dt, String dateformat) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return calendar2String(cal, dateformat);
    }

    public static final String date2StringDB(Date dt) {
        return date2String(dt, FORMAT_YYYYMMDD_HHMMSS);
    }

    public static final String date2StringDB_YYYYMMDD(Date dt) {
        return sampleDateToString(dt, FORMAT_YYYYMMDD);
    }

    public static final String calendar2String(Calendar cal, String dateformat) {
        StringBuffer ret = new StringBuffer();

        String separator = new String();
        int iSep = dateformat.indexOf('-');
        if (iSep > 0) {
            separator = "-";
        } else {
            iSep = dateformat.indexOf('/');
            if (iSep > 0) {
                separator = "/";
            }
        }

        if (iSep == 2) {
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.DATE)), 2, '0'));
            ret.append(separator);
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, '0'));
            ret.append(separator);
            ret.append(cal.get(Calendar.YEAR));
        } else if (iSep == 4) {
            ret.append(cal.get(Calendar.YEAR));
            ret.append(separator);
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, '0'));
            ret.append(separator);
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.DATE)), 2, '0'));
            ret.append(" ");
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)), 2, '0'));
            ret.append(":");
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MINUTE)), 2, '0'));
            ret.append(":");
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.SECOND)), 2, '0'));
            // ret.append(":");
            // ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MILLISECOND)),
            // 2, '0'));
        } else if (FORMAT_YYYYMMDD_NOSEP.equals(dateformat)) {
            ret.append(cal.get(Calendar.YEAR));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, '0'));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.DATE)), 2, '0'));
        } else if (FORMAT_YYYYMMDD_HHMMSS_NOSEP.equals(dateformat)) {
            ret.append(cal.get(Calendar.YEAR));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, '0'));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.DATE)), 2, '0'));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)), 2, '0'));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.MINUTE)), 2, '0'));
            ret.append(StringUtils.leftPad(String.valueOf(cal.get(Calendar.SECOND)), 2, '0'));
        } else {
            DateFormat dateFormatter = new SimpleDateFormat(dateformat);
            ret.append(dateFormatter.format(cal.getTime()));
        }

        return ret.toString();
    }

    public static String getDateYYYYMMDD_HHMMSS(Long millisecond) {
        Date currentDate = new Date(millisecond);
        DateFormat df = new SimpleDateFormat(FORMAT_YYYYMMDD_HHMMSS);
        return df.format(currentDate);
    }
    public static Date getDateFullFormat(Date date) {
      String dateLabel = getDateYYYYMMDD(date);
      dateLabel += " "+ DateTimeUtils.getDateLabel(DateTimeUtils.today(), "HH:mm:ss");
        return getDate(dateLabel, FORMAT_YYYYMMDD_HHMMSS);
    }
    public static Date getDateFullFormat(String date) {
    	try {
            String dateLabel = getDateYYYYMMDD(getDate(date));
            dateLabel += " "+ DateTimeUtils.getDateLabel(DateTimeUtils.today(), FORMAT_HH_MM_SS);
            return getDate(dateLabel, FORMAT_YYYYMMDD_HHMMSS);
		} catch (Exception e) {
		    return getDate(date, FORMAT_YYYYMMDD_HHMMSS); 
		}

      }
    public static String getDateYYYYMMDD(Long millisecond) {
        Date currentDate = new Date(millisecond);
        DateFormat df = new SimpleDateFormat(FORMAT_YYYYMMDD);
        return df.format(currentDate);
    }

    public static String getDateYYYYMMDD(Date date) {
        DateFormat df = new SimpleDateFormat(FORMAT_YYYYMMDD);
        return df.format(date);
    }

    /**
     * This for get only hour
     *
     * @return
     */

    public static int getCurrentHour() {
        return LocalDateTime.now().getHour();

    }

    public static int getHourFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.get(Calendar.HOUR);

    }

    public static int getMinutesFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.get(Calendar.MINUTE);

    }

    public static String todayFullNoSeparator() {
        return date2String(today(), FORMAT_YYYYMMDD_HHMMSS_NOSEP);
    }

    public static Date today() {
        return Calendar.getInstance().getTime();
    }

    public static String getSaGameDate(Date dtDate) {
        String result = date2String(addHour(dtDate, 2), FORMAT_YYYYMMDD_HHMMSS);
        return result;
    }

    public static String getSaGameDate(String dtDate) {
        Date result = addHour(string2Date(dtDate, FORMAT_YYYYMMDD_HHMMSS), 2);
        return date2String(result, FORMAT_YYYYMMDD_HHMMSS);
    }


    public static String toISO8601UTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static Date fromISO8601UTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);

        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static Date getDateAtBeginningOfMonth() {
    	return getDateAtBeginningOfMonth((Date)null);
    }

    public static Date getDateAtBeginningOfMonth(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        if (date !=null) {
        	calendar.setTime(date);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getDateAtBeginningOfMonth(final String dateLabel) {
        return getDateAtBeginningOfMonth(getDate(dateLabel));
    }

    public static Date getDateAtEndOfMonth() {
    	return getDateAtEndOfMonth((Date)null);
    }

    public static Date getDateAtEndOfLastMonth() {
        final Calendar calendar = Calendar.getInstance();
    	calendar.setTime(getDateAtBeginningOfLastMonth());
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }

    public static Date getDate(final String dateLabel) {
        return getDate(dateLabel, DEFAULT_DATE_FORMAT);
    }
    public static Date getDateDB(final String dateLabel) {
        return getDate(dateLabel, FORMAT_YYYYMMDD_HHMMSS);
    }
    public static Date getDateAtEndOfMonth(final Date date) {
        final Calendar calendar = Calendar.getInstance();
    	calendar.setTime(getDateAtBeginningOfMonth(date));
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }
    public static Date getDateAtBeginningOfLastMonth() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
    public static Long getDiffInDays(final Date date1, final Date date2) {
        if (date1 != null && date2 != null) {
            final Long diffInDays = (date1.getTime() - date2.getTime()) / (1000 * 60 * 60 * 24);
            return diffInDays;
        }
        return null;
    }
    public static Long getDiffSecond(final Date date1, final Date date2) {
        if (date1 != null && date2 != null) {
            final Long diffInDays = (date1.getTime() - date2.getTime()) / (1000);
            return diffInDays;
        }
        return null;
    }
    public static Date getDate(final String dateLabel, final String formatPattern) {
        if (dateLabel != null && formatPattern != null) {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat(formatPattern);
                return sdf.parse(dateLabel);
            }
            catch (ParseException pe) {
            	System.out.print(pe);
                return null;
            }
        }
        return null;
    }

    public static Date getDate(Date date) {
        return  DateTimeUtils.getDate(DateTimeUtils.getDateLabel(date, DateTimeUtils.FORMAT_YYYYMMDD), DateTimeUtils.FORMAT_YYYYMMDD);
    }
    public static Date getDateAtBeginningOfWeek(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        while (calendar.get(Calendar.DAY_OF_WEEK) != calendar.getFirstDayOfWeek()) {
            calendar.add(Calendar.DAY_OF_WEEK, -1);
        }
        return calendar.getTime();
    }
    
    public static boolean isThursDay(final Date date) {
        final GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
            return true;
        } else {
            return false;
        }

    }
    public static boolean isFriday(final Date date) {
        final GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            return true;
        } else {
            return false;
        }

    }
    public static String getDayCode() {
        final GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
        calendar.setTime(DateTimeUtils.getCurrentDate());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            return "MON";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
            return "THE";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
            return "THE";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
            return "WED";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
            return "THU";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            return "FRI";
        } else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            return "SAT";
        } else {
        	return "SUN";
        }
    
    }
    public static boolean isSunday(final Date date) {
        final GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        } else {
            return false;
        }

    }

    //long diff = d2.getTime() - d1.getTime();
    public static Boolean isBeforeElapsedTime(final Long startTime, final Integer secs) {
        Long currentTime = System.currentTimeMillis();
        Long elapsedTime = currentTime - startTime;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
        if(seconds > secs) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public static int age(Date birthday) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(birthday));
        int d2 = Integer.parseInt(formatter.format(today()));
        int age = (d2-d1)/10000;
        return age;
    }
}
