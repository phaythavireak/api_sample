package com.example.demo.utils;

import com.example.demo.model.share.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public final class EntityUtil {

    public static final String getName(Class<? extends BaseEntity> type) {
        // All main entities have simple one word names, so this is sufficient. Metadata
        // could be added to the class if necessary.
        return type.getSimpleName();
    }

    public static Class<?> getClazzByName(String name) {
        List<Class> classes = getListEntity();
        for (Class clazs : classes
        ) {
            if (name.equals(clazs.getSimpleName())) {
                return clazs;
            }
        }
        return null;
    }

    public static List<Class> getListEntity() {
        List<Class> result = new ArrayList<>();
        List<Class> classes = ClassFinderUtils.getClasses("com.kh.fin.core.domain.model");
        return classes;
    }


}
