package com.example.demo.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Currency;
import java.util.Locale;

/**
 * Money utility to calculate money in double data type
 * <br/>
 * This takes care of currency decimal place issue
 * 
 * @author kimsur.seang
 *
 */
public abstract class MoneyUtils {
	
	private static Currency defaultCurrency = Currency.getInstance(Locale.US);
	
	private static RoundingMode defaultRoundingMode = RoundingMode.HALF_EVEN;
	
	
	/**
	 * Sum between double number1 and array of doubles number2
	 * @param number1
	 * @param number2
	 * @return
	 */
	public static double sum(double number1, double...number2) {
		
		if (number2.length == 0) {
			return number1;
		} else if (number2.length == 1) {
			return sum(number1, number2[0], defaultCurrency);
		} else {
			return sum(sum(number1, number2[0], defaultCurrency), 
					copyOfRange(number2, 1));
		}
	}
	
	/**
	 * Sum between two doubles
	 * <br/>
	 * augend + addend
	 * 
	 * @param augend
	 * @param addend
	 * @param currency
	 * @return
	 */
	public static double sum(double augend, double addend, Currency currency) {
		
		double sum = BigDecimal.valueOf(augend)
				.add(BigDecimal.valueOf(addend))
				.setScale(currency.getDefaultFractionDigits(), defaultRoundingMode)
				.doubleValue();
		
		return sum;
	}
	
	
	/**
	 * Subtract two doubles 
	 * <br/>
	 * minuend - subtrahend
	 * 
	 * @param minuend
	 * @param subtrahend
	 * @return
	 */
	public static double subtract(double minuend, double subtrahend) {
		return subtract(minuend, subtrahend, defaultCurrency);
	}
	
	/**
	 * Subtract two doubles 
	 * <br/>
	 * minuend - subtrahend
	 * 
	 * @param minuend
	 * @param subtrahend
	 * @param currency
	 * @return
	 */
	public static double subtract(double minuend, double subtrahend, Currency currency) {
		
		double difference = BigDecimal.valueOf(minuend)
				.subtract(BigDecimal.valueOf(subtrahend))
				.setScale(currency.getDefaultFractionDigits(), defaultRoundingMode)
				.doubleValue();
		
		return difference;
	}
	
	
	
	/**
	 * Multiply number1 by array of number2
	 * 
	 * @param number1
	 * @param number2
	 * @return
	 */
	public static double multiply(double number1, double...number2) {
		
		if (number2.length == 0) {
			return number1;
		} else if (number2.length == 1) {
			return multiply(number1, number2[0], defaultCurrency);
		} else {
			return multiply(multiply(number1, number2[0], defaultCurrency), 
					copyOfRange(number2, 1));
		}
	}
	
	
	/**
	 * Multiply two doubles
	 * <br/>
	 * multiplier * multiplicand
	 * 
	 * @param multiplier
	 * @param multiplicand
	 * @param currency
	 * @return
	 */
	public static double multiply(double multiplier, double multiplicand, Currency currency) {
		
		double product = BigDecimal.valueOf(multiplier)
				.multiply(BigDecimal.valueOf(multiplicand))
				.setScale(currency.getDefaultFractionDigits(), defaultRoundingMode)
				.doubleValue();
		
		return product;
	}
	
	
	/**
	 * Divide two doubles
	 * <br/>
	 * dividend / divisor
	 * 
	 * @param dividend
	 * @param divisor
	 * @return
	 */
	public static double divide(double dividend, double divisor) {
		return divide(dividend, divisor, defaultCurrency);
	}
	
	/**
	 * Divide two doubles
	 * <br/>
	 * dividend / divisor
	 * @param dividend
	 * @param divisor
	 * @param currency
	 * @return
	 */
	public static double divide(double dividend, double divisor, Currency currency) {
		
		double quotient = BigDecimal.valueOf(dividend)
				.divide(BigDecimal.valueOf(divisor))
				.setScale(currency.getDefaultFractionDigits(), defaultRoundingMode)
				.doubleValue();
		
		return quotient;
	}

	
	/**
	 * Compare two doubles
	 * @param number1
	 * @param number2
	 * @return
	 */
	public static int compare(double number1, double number2) {
		return BigDecimal.valueOf(number1)
				.compareTo(BigDecimal.valueOf(number2));
	}
	
	/**
	 * Round double number
	 * 
	 * @param number
	 * @return
	 */
	public static double round(double number) {
		return BigDecimal.valueOf(number)
		.setScale(defaultCurrency.getDefaultFractionDigits(), defaultRoundingMode)
		.doubleValue();
	}
	
	public static BigDecimal roundBigDecimal(BigDecimal bigDecimal) {
		return bigDecimal.setScale(defaultCurrency.getDefaultFractionDigits(), defaultRoundingMode);
	}
	
	
	/**
	 * Split array to a new one from "from" index to the end of old array
	 * @param array
	 * @param from
	 * @return
	 */
	private static double[] copyOfRange(double[] array, int from) {
		return Arrays.copyOfRange(array, from, array.length);
	}
	
	/**
	 * Double to {@link BigDecimal}
	 * @param number
	 * @return
	 */
	public static BigDecimal toBigDecimal(double number) {
		return BigDecimal.valueOf(number);
	}
}
