package com.example.demo.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;

import java.security.SecureRandom;

/**
 * Password encode utility
 * <br/>
 * It will use BCrypt encryption
 * 
 * @author kimsur.seang
 *
 */
public abstract class PasswordEncoder {
	protected static int DEFAULT_LENGTH 		= 13;
	protected static String DEFAULT_SALT 		= "casino'secure";
	protected static String PASSWORD_SALT 		= "password'salt";
	protected static Long TIME_SALT 		= System.currentTimeMillis();
	protected static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(DEFAULT_LENGTH, new SecureRandom(TIME_SALT.toString().getBytes()));
	
	
	public static String encode(String rawPassword) {
		return encoder.encode(rawPassword);
	}
	
	public static boolean matches(String rawPassword, String encodedPassword) {
		return encoder.matches(rawPassword, encodedPassword);
	}

	public static String encodePassword(String password) {
		Assert.notNull(password,"Password could not be null");
		return Base64Utils.encodeToString(password.getBytes());
	}

	public static String decodePassword(String encodeString) {
		Assert.notNull(encodeString,"Password could not be null");
		String decodePassword = new String(Base64Utils.decodeFromString(encodeString));
		String [] keys = decodePassword.split("\\.", -1);
		return decodePassword;
	}

	public static String generateRandomPassword(int len) {
		final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		return RandomStringUtils.random(len, chars);
	}

	public static String encodePasswordDefaultPass() {
	   String defaultPwd = generateRandomPassword(6);
		return encodePassword(defaultPwd);
	}
}
