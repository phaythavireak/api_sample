package com.example.demo.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sok vina
 *
 */
public class PositionType {
	 public static String RIGHT_HEAD = "RIGHT_HEAD";
	 public static String RIGHT_CONTENT = "RIGHT_CONTENT";
	 public static String SITE_MAP = "SITE_MAP";
	 public static String FOOTER = "FOOTER";
	 
	 public static List<String> list() {
		 List<String> list = new ArrayList<>();
		 list.add(RIGHT_HEAD);
		 list.add(RIGHT_CONTENT);
		 list.add(SITE_MAP);
		 list.add(FOOTER);
		 return list;
	 }
}
