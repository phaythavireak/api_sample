package com.example.demo.utils;

import lombok.Data;

import java.util.List;

/**
 * 
 * @author Sok Vina
 *
 */
@Data
public class QueryResult {
	private List<Object[]> list;
	private Long count = 0l;
}
