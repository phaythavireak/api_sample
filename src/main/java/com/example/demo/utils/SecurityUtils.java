package com.example.demo.utils;

import com.example.demo.model.security.User;
import com.example.demo.model.security.UserProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * Created by Kimsuor SEANG
 * Date  : 8/31/2017
 * Name  : 2:42 PM
 * Email : k.seang@gl-f.com
 */
@Slf4j
public class SecurityUtils {

    private static final String SPRING_PREF_ROLE = "ROLE_";
    public static final String ROLE_SUP_ADMIN = "SUP_ADMIN";
    public static final String ROLE_ADMIN = "ADMIN";
    
    public static final String ROLE_CASHIER = "SALE";
    public static final String ROLE_BM = "BM";
    public static final String ROLE_ACC = "ACC";
    public static final String ROLE_ACC_MGT = "ACC_MGT";


    public static boolean isSupAdmin() {
        if (ROLE_SUP_ADMIN.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSale(User user) {
        if (user.getProfile().getCode().equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isBM(User user) {
        if (user.getProfile().getCode().equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isAcc(User user) {
        if (user.getProfile().getCode().equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isAccMgt(User user) {
        if (user.getProfile().getCode().equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isSale() {
        if (ROLE_CASHIER.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isBM() {
        if (ROLE_BM.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isAcc() {
        if (ROLE_ACC.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isAccMgt() {
        if (ROLE_ACC_MGT.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isAdmin() {
        if (ROLE_ADMIN.equals(getCurrentUserProfileFromLogin().getCode())) {
            return true;
        } else {
            return false;
        }
    }
    
  

    public static String[] fullAccess() {
        return new String[]{
                ROLE_SUP_ADMIN,ROLE_ADMIN,ROLE_ACC_MGT,ROLE_BM,ROLE_ACC,ROLE_CASHIER
        };
    }

    public static String[] superAdmin() {
        return new String[]{
                ROLE_SUP_ADMIN
        };
    }

    public static Long getUserIdLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.isAuthenticated()) {
            User currentUser = (User) auth.getPrincipal();
            return currentUser.getId();
        } else {
            return null;
            // throw new IllegalStateException("User not Autheicated.");
        }
    }

    /**
     * Checks if the user is logged in.
     *
     * @return true if the user is logged in. False otherwise.
     */
    public static boolean isUserLoggedIn() {
        return isUserLoggedIn(SecurityContextHolder.getContext().getAuthentication());
    }

    public static boolean isUserLoggedIn(Authentication authentication) {
        return authentication != null
                && !(authentication instanceof AnonymousAuthenticationToken);
    }

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user
     */
    public static Optional<String> getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> {
                    if (authentication.getPrincipal() instanceof UserDetails) {
                        UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                        return springSecurityUser.getUsername();
                    } else if (authentication.getPrincipal() instanceof String) {
                        return (String) authentication.getPrincipal();
                    }
                    return null;
                });
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     */
    public static boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> authentication.getAuthorities().stream()
                        .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(AuthoritiesConstants.ANONYMOUS)))
                .orElse(false);
    }

    /**
     * If the current user has a specific authority (security UserProfile).
     * <p>
     * The name of this method comes from the isUserInRole() method in the Servlet API
     *
     * @param authority the authority to check
     * @return true if the current user has the authority, false otherwise
     */
    public static boolean isCurrentUserInRole(String authority) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> authentication.getAuthorities().stream()
                        .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority)))
                .orElse(false);
    }

    /**
     * Get User session from Spring Security context
     *
     * @return
     */
    public static User getUserSession() {
        User userSession = null;

        try {
            SecurityContext context = SecurityContextHolder.getContext();

            Authentication authentication = context.getAuthentication();
            if (authentication != null && authentication.isAuthenticated()) {
                Object principal = authentication.getPrincipal();

                if (principal instanceof User) {
                    userSession = (User) principal;
                } else if (principal instanceof UserDetails) {
                    userSession = new User();
                    BeanUtils.copyNonNullProperties(principal, userSession);
                }

            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }

        return userSession;
    }

    public static UserProfile getCurrentUserProfileFromLogin() {
        User user = getUserSession();
        return user.getProfile();
    }


    public static Long getSessionUserId() {
        User userSession = getUserSession();
        if (userSession != null) {
            return userSession.getId();
        } else {
            return null;
        }
    }


    /**
     * Get servlet request from {@link RequestContextHolder}
     *
     * @return
     */
    public static HttpServletRequest getServletRequest() {
        ServletRequestAttributes requestServletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();

        return requestServletRequestAttributes != null ?
                requestServletRequestAttributes.getRequest() : null;
    }

    /**
     * Internally check ip is local IP
     *
     * @param ip
     * @return
     */
    private static boolean isLocalHost(String ip) {
        return "127.0.0.1".equals(ip);
    }


    /**
     * Get Client IP from RequestServlet
     *
     * @param request
     * @return
     */
    public static String getRequestHostIp(HttpServletRequest request) {
        String ip = request.getRemoteAddr();

        if (isLocalHost(ip)) {
            try {
                ip = Inet4Address.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                log.warn("Can't detect Local IP", e);
            }
        }

        return ip;
    }


}
