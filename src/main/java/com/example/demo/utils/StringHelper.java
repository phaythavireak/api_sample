package com.example.demo.utils;



import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringHelper {

    private static String cacheGeneratedValue = null;


    private static final String SECRET_KEY = "client_SECRET";
    private static final String SALT = "ApiChannel@#4!!!!";
    private static final String SECRET_RSA_KEY = "||||";

    /**
     * Generate String unique value
     *
     * @return
     */
    public synchronized static String generateUnique() {
        SimpleDateFormat dtFormatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String generatedValue = dtFormatter.format(new Date());

        if (generatedValue.equals(cacheGeneratedValue)) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            generatedValue = generateUnique();
        }

        cacheGeneratedValue = generatedValue;

        return generatedValue.toString();
    }

    public static boolean isHttpsAccess(String url) {
        if (url.contains("https://")) {
            return true;
        } else {
            return false;
        }
    }

    public static String toSecret(String phone, String prefixName) {
        return phone.concat(prefixName).concat("otp");
    }

    public static String random() {
        return RandomStringUtils.random(10, true, true);
    }

    public static String getUniqueId(String enityId) {
        String prefixName = StringUtils.leftPad(enityId, 8, "0");
        String day = StringUtils.leftPad(String.valueOf(DateTimeUtils.getDay(new Date())), 2, "0");
        String hours = StringUtils.leftPad(String.valueOf(DateTimeUtils.getCurrentHour()), 2, "0");
        StringBuilder result = new StringBuilder(prefixName).append(day).append(hours)
                .append(RandomStringUtils.random(6, false, true));
        return result.toString();
    }

    public static String getTransactionIdUniq(String userName) {
        String prefixName = StringUtils.leftPad(userName, 10, "0");
        String day = StringUtils.leftPad(String.valueOf(DateTimeUtils.getDay(new Date())), 2, "0");
        String hours = StringUtils.leftPad(String.valueOf(DateTimeUtils.getCurrentHour()), 2, "0");
        StringBuilder result = new StringBuilder(prefixName).append(day).append(hours)
                .append(RandomStringUtils.random(6, false, true));
        return result.toString();
    }

    public static String getTransactionIdUniq() {
        String date = DateTimeUtils.date2String(new Date(), DateTimeUtils.FORMAT_YYYYMMDD_NOSEP);
        String day = StringUtils.leftPad(String.valueOf(DateTimeUtils.getDay(new Date())), 2, "0");
        String hours = StringUtils.leftPad(String.valueOf(DateTimeUtils.getCurrentHour()), 2, "0");
        StringBuilder result = new StringBuilder(date).append(RandomStringUtils.random(6, false, true));
        return result.toString();
    }

    public static String getUserIdUniq(String userName, String gameCategory) {
        String ranNumb = RandomStringUtils.random(4, false, true);
        String result =  StringUtils.substring(userName, userName.length() - 6, userName.length())
                + gameCategory + ranNumb;
        return result;
    }

    public static String makeRSAKeyRequest(String clientRSA) {
        StringBuilder result = new StringBuilder(clientRSA).append(SECRET_RSA_KEY).append(System.currentTimeMillis());
        return result.toString();
    }

}
