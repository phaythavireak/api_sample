package com.example.demo.utils;



import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Sok.vina 03-06-2015 This static class for converting value double,
 * string, float,... and validated null pointer exception
 */
public class ValueUtils {

    public static String CURRENCY_PREFIX = "$";
    public static int DEFAULT_DECIMAL = 2;
    public static int DEFAULT_BIG_DECIMAL = 4;

    public static String getString(String value) {
        if (value != null) {
        	if(value.equals("null")) {
        		value = "";
        	}
            return value;
        }
        return "";
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(String value, String defaultValue) {
        if (value != null) {
            return value;
        }
        return defaultValue;
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Double value) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static String getString(String key, Map mapValue) {

        if (mapValue.containsKey(key)) {
            return String.valueOf(mapValue.get(key));
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Long value) {
        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return 0
     */
    public static String getStringWithZero(Double value) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return "0";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getStringWithDefaultVal(Double value, String val) {

        if (value != null && value != 0d) {
            return value.toString();
        } else {
            return val;
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Integer value) {

        if (value != null && value != 0) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static String getStringWithZero(Integer value) {

        if (value != null && value != 0 && value > 0) {
            return value.toString();
        } else {
            return "0";
        }
    }

    /**
     * @param value
     * @return
     */
    public static String getStringWithNA(String value) {

        if (value != null && !value.trim().isEmpty()) {
            return value;
        } else {
            return "N/A";
        }
    }

    public static String getInteger(Integer value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static String getDoubles(Double value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    public static Double getDoubleDecimal2DigitRoundDown(Double value) {
        if (value != null) {
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.DOWN);
            return getDouble(df.format(value));
        }
        return 0d;
    }

    public static boolean isDecimalGreaterThen(Double value, int decimalPlace) {
        if (value != null) {
            return BigDecimal.valueOf(value).scale() > decimalPlace;
        }
        return false;
    }

    /**
     * @param value
     * @return
     */
    public static Long getLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (Exception ex) {
            return 0L;
        }
    }

    public static String getStrings(Integer value) {

        if (value != null) {
            return value.toString();
        } else {
            return "";
        }
    }

    /**
     * @param value
     * @return string
     */
    public static String getString(Float value) {

        if (value == 0f || value == 0d || value == null) {
            return "";
        } else {
            return value.toString();
        }
    }

    /**
     * @param value
     * @return double
     */
    public static Double getDouble(String value) {
        try {
            return !Double.isNaN(Double.parseDouble(value)) ? Double.parseDouble(value) : 0d;
        } catch (NumberFormatException | NullPointerException nfe) {
            return 0d;
        }
    }


    /**
     * @param value
     * @return double
     */
    public static Double getDouble(Double value) {
        if (value != null) {
            return getDouble(getString(value));
        } else {
            return 0d;
        }
    }

    public static Double getDouble(BigDecimal value) {
        if (value != null) {
            return getDoubleFormat(value.doubleValue(), 4);
        } else {
            return 0d;
        }
    }

    public static Double getDouble(String key, Map mapValue) {
        if (mapValue.containsKey(key)) {
            Object value = mapValue.get(key);
            if (value instanceof Double) {
                return getDouble((Double) value);
            } else if (value instanceof String) {
                return getDouble((String) mapValue.get(key));
            } else if (value instanceof Integer) {
                return getDouble((Integer) value);
            } else if (value instanceof BigDecimal) {
                return getDouble((BigDecimal) value);
            } else if (value instanceof Long) {
                return (Double) value;
            } else {
                return 0d;
            }

        } else {
            return 0d;
        }
    }


    /**
     * @param value
     * @return double
     */
    public static Double getDouble(Integer value) {
        try {
            return getDouble(value.toString());
        } catch (Exception ex) {
            return 0d;
        }
    }

    /**
     * @param value
     * @return float
     */
    public static Float getFloat(String value) {

        if (value != null && !value.isEmpty()) {
            return Float.parseFloat(value);
        } else {
            return 0f;
        }
    }

    /**
     * @param value
     * @return
     */
    public static Integer getInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static double getHoursKaraoke(Long minutes, int splitTime) {
        double hour = 0l;
        if (minutes != null) {
            if (minutes >= splitTime && minutes < 30 + splitTime) {
                hour = 0.5;
            } else if (minutes >= 30 + splitTime) {
                hour = 1d;
            }
        }
        return hour;
    }

    public static String getTime(Long millis) {
        if (millis != null) {
            String s = "" + (millis / 1000) % 60;
            String m = "" + (millis / (1000 * 60)) % 60;
            String h = "" + (millis / (1000 * 60 * 60)) % 24;
            String d = "" + (millis / (1000 * 60 * 60 * 24));
            return d + "d " + h + "h:" + m + "m:" + s + "s";
        }
        return "N/A";
    }

   /* public static String getTimeDisplay(Date start) {
        Long millis = getProcessTime(start);
        if (millis != null) {
            Long w = (millis / (1000 * 60 * 60 * 24 * 7));
            if (w != null && w > 0) {
                return w + English.plural(" week", w.intValue()) + " ago";
            }
            Long d = (millis / (1000 * 60 * 60 * 24));
            if (d != null && d > 0) {
                return d + English.plural(" day", d.intValue()) + " ago";
            }
            Long h = (millis / (1000 * 60 * 60)) % 24;
            if (h != null && h > 0) {
                return h + English.plural(" hour", h.intValue()) + " ago";
            }
            Long m = (millis / (1000 * 60)) % 60;
            if (m != null && m > 0) {
                return m + English.plural(" minute", m.intValue()) + " ago";
            }
            Long s = (millis / 1000) % 60;
            if (s != null && s > 5) {
                return s + English.plural(" second", s.intValue()) + " ago";
            } else if (s != null && s >= 0) {
                return "now";
            }
        }
        return "N/A";
    }*/

    private static Long getProcessTime(Date start) {
        long times = 0L;
        Date end = DateTimeUtils.getCurrentDateTime();
        times = end.getTime() - start.getTime();
        return times;
    }

    public static Integer getInt(Double value) {

        if (value != null) {
            return value.intValue();
        } else {
            return 0;
        }
    }

    public static Integer getInt(int value) {

        if (value > 0) {
            return value;
        } else {
            return 0;
        }
    }
    public static Integer getInt(Integer value) {

        if (value != null) {
            return value.intValue();
        } else {
            return 0;
        }
    }

    public static String getStringFormat(String value) {
        String strValue = "";
        if (value != null) {
            try {
                Double dobValue = Double.parseDouble(value);
                strValue = dobValue.toString();
            } catch (Exception e) {
                strValue = value;
            }

        }
        return strValue;
    }

    public static String getStringFromArray(List<String> list) {
        return list.stream().collect(Collectors.joining(","));
    }

    public static List<String> getArrayFromString(String listString) {
        if (listString != null) {
            List<String> list = Arrays.asList(listString.split(","));
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    list.set(i, list.get(i).trim());
                }
                return list;
            }
        }
        return Arrays.asList();
    }

    public static <T> List<T> getDeletedItems(List<T> oldItems, List<T> newItems) {
        if (newItems == null || newItems.isEmpty()) {
            return oldItems;
        } else if (oldItems != null) {
            return oldItems.stream().filter(item -> !newItems.contains(item)).collect(Collectors.toList());
        }
        return null;
    }

    public static boolean isNullOrEmpty(String object) {
        if (object == null || object.isEmpty()) {
            return true;
        }
        return false;
    }

    public static double round(double value, int places) {
        if (value == 0) return 0;
        if (places < 0) {
            places = DEFAULT_DECIMAL;
        }
        value = getDouble(value);
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static String formatDecimal(Double value, int dec) {
        value = getDouble(value);
        value = round(value, dec);
        return String.format("%." + dec + "f", value);
    }

    public static Double getDoubleFormat(Double value, int dec) {
        if (value == null) {
            value = 0D;
        }
        value = round(value, dec);
        return getDouble(String.format("%." + dec + "f", value));
    }

    public static Double getDoubleFormat(Double value) {
        int dec = 2;
        if (value == null) {
            value = 0D;
        }
        value = round(value, dec);
        return getDouble(String.format("%." + dec + "f", value));
    }

    public static Double getDoubleFormat(String valueStr, int dec) {
        double value = round(getDouble(valueStr), dec);
        return getDouble(String.format("%." + dec + "f", value));
    }

    public static String formatDecimal(Double value) {
        return formatDecimal(value, DEFAULT_DECIMAL);
    }
    public static String format2Digit(Double value) {
        final DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.DOWN);
        Double val = getDouble(value);
        return df.format(val);
    }
    public static String formatCurrency(Double value) {
        return formatCurrency(value, DEFAULT_DECIMAL);
    }

    public static String formatCurrency(Double value, int dec) {
        return formatCurrency(value, null, dec);
    }

    public static String formatCurrency(Double value, String prefix, int dec) {
        if (prefix == null) {
            prefix = CURRENCY_PREFIX;
        }
        if (dec < 0) {
            int mul = getInt(String.format("1%0" + Math.abs(dec) + "d", 0));
            if (mul > 0) {
                value = value / mul;
                value = round(value, 0);
                value = value * mul;
            }
            dec = 1;
        }
        value = getDouble(value);
        value = round(value, dec);
        String defaultFormat = prefix + "###,###";
        switch (dec) {
            case 1:
                defaultFormat += ".#";
                break;
            default:
                defaultFormat += ".##";
        }
        DecimalFormat decimalFormat = new DecimalFormat(defaultFormat);
        return decimalFormat.format(value);
    }

    public static Boolean getBooleanValue(Boolean value) {
        if (value != null) {
            return value;
        } else {
            return Boolean.FALSE;
        }
    }

    public static String getBigDecimal(BigDecimal value, int scale) {
        if (value == null) {
            value = new BigDecimal(0).setScale(scale);
            return value.toString();
        } else {
            return value.setScale(scale).toString();
        }
    }

    public static String getBigDecimal(BigDecimal value) {
        return getBigDecimal(value, DEFAULT_BIG_DECIMAL);
    }


    public static BigDecimal getBigDecimal(String value) {
        if (!StringUtils.isEmpty(value)) {
            return new BigDecimal(value);
        } else {
            return new BigDecimal(0);
        }
    }


    public static BigDecimal getBigDecimal(Double value) {
        if (value != null) {
            return new BigDecimal(value).setScale(4, RoundingMode.HALF_UP);
        } else {
            return new BigDecimal(0);
        }
    }

    public static BigDecimal addBigDecimalValue(BigDecimal value1, BigDecimal value2) {
        if (value1 == null) {
            value1 = new BigDecimal(0);
        }
        if (value2 == null) {
            value2 = new BigDecimal(0);
        }
        BigDecimal result = value1.add(value2);
        result.setScale(4, RoundingMode.HALF_UP);
        return result;
    }

    public static BigDecimal subtractBigDecimalValue(BigDecimal value1, BigDecimal value2) {
        if (value1 == null) {
            value1 = new BigDecimal(0);
        }
        if (value2 == null) {
            value2 = new BigDecimal(0);
        }
        BigDecimal result = value1.subtract(value2);
        result.setScale(4, RoundingMode.HALF_UP);
        return result;
    }

    public static BigDecimal multiBigdecimalVal(BigDecimal value1, BigDecimal value2) {
        if (value1 == null) {
            value1 = new BigDecimal(0);
        }
        if (value2 == null) {
            value2 = new BigDecimal(0);
        }
        BigDecimal result = value1.multiply(value2);
        result.setScale(4, RoundingMode.HALF_UP);
        return result;
    }

    public static String convertToString(List<String> values) {
        if (values != null && !values.isEmpty()) {
            String strId = "";
            int index = 0;
            for (String item : values) {
                index++;
                if (index == values.size()) {
                    strId += "'" + item + "'";
                } else {
                    strId += "'" + item + "'" + ",";
                }
            }
            return strId;
        }
        return null;
    }
    public static String convertToStringForSMS(List<String> values) {
        if (values != null && !values.isEmpty()) {
            String strId = "";
            int index = 0;
            for (String item : values) {
                index++;
                if (index == values.size()) {
                    strId += "" + item + "";
                } else {
                    strId += "" + item + "" + ",";
                }
            }
            return strId;
        }
        return null;
    }
    public static String convertToLong(List<Long> values) {
        if (values != null && !values.isEmpty()) {
            String strId = "";
            int index = 0;
            for (Long item : values) {
                index++;
                if (index == values.size()) {
                    strId += "" + item + "";
                } else {
                    strId += "" + item + "" + ",";
                }
            }
            return strId;
        }
        return null;
    }
    
    public static String convertToStringByLong(List<Long> values) {
        if (values != null && !values.isEmpty()) {
            String strId = "";
            int index = 0;
            for (Long item : values) {
                index++;
                if (index == values.size()) {
                    strId += "" + item + "";
                } else {
                    strId += "" + item + "" + ",";
                }
            }
            return strId;
        }
        return null;
    }
    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
