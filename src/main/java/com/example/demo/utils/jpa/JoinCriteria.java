package com.example.demo.utils.jpa;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class JoinCriteria<E> implements Serializable {

    private String alias;
    private String propertyField;
    //private Class<?> rootClazz;
    private String  joinEntity;
    private JoinType joinType;
    private Object jointValue;
    private SearchOperation searchOperation;
    public JoinCriteria() {
    }
    public JoinCriteria(String joinEntity, String propertyField, Object jointValue, JoinType joinType) {
        this.propertyField = propertyField;
        this.joinEntity = joinEntity;
        this.joinType = joinType;
        this.jointValue = jointValue;
    }
    public JoinCriteria(String joinEntity, String propertyField, Object jointValue, JoinType joinType, SearchOperation searchOperation) {
        this.propertyField = propertyField;
        this.joinEntity = joinEntity;
        this.joinType = joinType;
        this.jointValue = jointValue;
        this.searchOperation = searchOperation;
    }

    public Join buildJoinCriterial(Root<?> rootEntry) {
        Join<?,E>  result = rootEntry.join(joinEntity,joinType);
        return result;
    }
}
