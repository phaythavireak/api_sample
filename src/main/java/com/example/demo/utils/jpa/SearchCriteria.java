package com.example.demo.utils.jpa;

import lombok.Data;

import java.io.Serializable;

@Data
public class SearchCriteria implements Serializable {

    private String key;
    private Object value;
    private SearchOperation operation;

    public SearchCriteria() {
    }

    public SearchCriteria(String key, Object value, SearchOperation operation) {
        this.key = key;
        this.value = value;
        this.operation = operation;
    }
}
