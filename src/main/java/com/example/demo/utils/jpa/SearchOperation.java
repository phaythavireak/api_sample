package com.example.demo.utils.jpa;

public enum  SearchOperation {

    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_EQUAL,
    LESS_THAN_EQUAL,
    NOT_EQUAL,
    EQUAL,
    MATCH,
    MATCH_START,
    MATCH_END,
    IN,
    NOT_IN,
    BETWEEN;


   /* public static final String[] SIMPLE_OPERATION_SET =
            { ":", "!", ">", "<", "~" };
    public static SearchOperation getSimpleOperation(final char input)
    {
        switch (input) {
            case ':': return EQUALITY;
            case '!': return NEGATION;
            case '>': return GREATER_THAN;
            case '<': return LESS_THAN;
            case '~': return LIKE;
            default: return null;
        }
    }*/
}
