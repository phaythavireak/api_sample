package com.example.demo.utils.jpa;

import com.example.demo.vo.PageableRequestVO;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Data
public class SpecificSearchCriteria implements Serializable {

    private static final long serialVersionUID = 460908657376018235L;

    List<SearchCriteria> searchCriteria;

    List<JoinCriteria> joinCriteria;
    
    List<SearchCriteria> searchORCriterias;
    
    PageableRequestVO pageableRequestVO;



    public void addCriteria(SearchCriteria criteria) {
        if(searchCriteria == null) {
            searchCriteria = new ArrayList<>();
        }
        searchCriteria.add(criteria);
    }

    public void addJoinType(JoinCriteria joinCriteria) {
        if(this.joinCriteria == null) {
            this.joinCriteria = new ArrayList<JoinCriteria>();
        }
        this.joinCriteria.add(joinCriteria);
    }
    
	public void addORCriteria(SearchCriteria... criteria) {
		if (searchORCriterias == null) {
			searchORCriterias = new ArrayList<>();
		}
		if (criteria != null) {
			for (SearchCriteria item : criteria) {
				searchORCriterias.add(item);
			}
		}
	}
}
