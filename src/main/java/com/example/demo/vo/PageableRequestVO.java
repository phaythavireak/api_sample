package com.example.demo.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.validation.constraints.Min;

/**
 * Represent Pageable Data to filter on Database
 * @author kimsur.seang
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableRequestVO implements Serializable {
	/**
	 * First index will start by 0 
	 */
	public static final Integer DEFAULT_PAGE 					= 1;
	//public static final Integer DEFAULT_RPP 					= 10;
	public static final Integer DEFAULT_SIZE = 10 ;
	public static final String DEFAULT_SORT="DESC";
	public static final String DEFAULT_SORT_FIELD="id";
	
	@ApiParam(value = "Page index, start from 1", defaultValue = "1")
	@Min(0)
	private Integer page = DEFAULT_PAGE;
	
	@ApiParam(value = "Limit size of  count record per page", defaultValue = "10")
	@Min(1)
	private Integer size = DEFAULT_SIZE;
	@ApiParam(value = "Sort  index, start with default sorted DESCENDING", defaultValue = "DESC")
	private String sort = DEFAULT_SORT;
	@ApiParam(value = "Sort field name  index, start with default is ID", defaultValue = "id")
	private String sortFieldName = DEFAULT_SORT_FIELD;

	public Integer getPage() {
		return page == null ? 0 : page - 1;
	}

	@JsonIgnore
	public Integer getOrigValuePage() {
		return page ;
	}

	@JsonProperty("activate")
	private Boolean activate = Boolean.TRUE;

}
