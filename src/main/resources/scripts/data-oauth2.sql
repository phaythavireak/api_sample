INSERT INTO oauth_client_details
    (client_id, resource_ids,client_secret, scope, authorized_grant_types,
    web_server_redirect_uri, authorities, access_token_validity,
    refresh_token_validity, additional_information, autoapprove)
VALUES ('client-mobile','resource_identity', '$2a$10$aPD0NLrZ2u/Gr2tn/exgnOTb9pvnlKB42cduPctOZA8Nurg1xXWKy', 'trust,read,write',
    'password,authorization_code,refresh_token,implicit,client_credentials', null, 'ROLE_CLIENT,ROLE_APP', 525600, 525600, null, true)
ON CONFLICT (client_id) DO NOTHING;

INSERT INTO oauth_client_details
(client_id, resource_ids,client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES ('client-web','resource_identity', '$2a$10$aPD0NLrZ2u/Gr2tn/exgnOTb9pvnlKB42cduPctOZA8Nurg1xXWKy', 'trust,read,write',
                     'password,authorization_code,refresh_token,implicit,client_credentials', null, 'ROLE_USER,ROLE_ADMIN', 300, 420, null, true)
ON CONFLICT (client_id) DO NOTHING;


-- INSERT INTO table_user (usr_code, usr_name, usr_expiration_date, usr_pwd, usr_answer1, usr_answer2, usr_question1, usr_question2, usr_login)
-- VALUES (NULL, 'adminstrator', '2018-02-01 15:02:54', '$2a$10$aPD0NLrZ2u/Gr2tn/exgnOTb9pvnlKB42cduPctOZA8Nurg1xXWKy', NULL, NULL, NULL, NULL, 'admin')
-- ON CONFLICT (usr_login) DO NOTHING;
--
-- INSERT INTO table_profile (pro_code, pro_desc)
-- VALUES ('USER', 'user') ON CONFLICT (pro_code) DO NOTHING;
-- INSERT INTO table_profile (pro_code, pro_desc)
-- VALUES ('ADMIN', 'administrator') ON CONFLICT (pro_code) DO NOTHING;
--
-- INSERT INTO table_user_profile (usr_id, pro_id) VALUES ('1', '2');



